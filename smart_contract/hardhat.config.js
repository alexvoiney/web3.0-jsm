require("@nomiclabs/hardhat-waffle");

module.exports = {
  solidity: "0.8.0",
  networks: {
    ropsten: {
      url: "https://eth-ropsten.alchemyapi.io/v2/Ae50BLhejs-do4zZVC5N5ZwjocEVBgcK",
      accounts: [
        "3beb4fc5ab4a8d5e22e6e1d882689a4c7002fc0732c1978df3c0d375e934b9a0",
      ],
    },
  },
};
